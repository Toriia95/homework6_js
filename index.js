
function dateDiff(d) {
    let modifierYear = 0;
    const today = new Date();

    const yearDiff = today.getFullYear() - d.getFullYear();

    if (today.getMonth() < d.getMonth()) {
        modifierYear = 1;
    } else if (today.getMonth() === d.getMonth()) {
        if (today.getDay() < d.getDay()) {
            modifierYear = 1;
        }
    }

    return yearDiff - modifierYear;
}

function getLogin() {
    const firstNameLetter = this.firstName[0].toLocaleLowerCase();
    const surnameInLowerCase = this.lastName.toLocaleLowerCase();

    return `${firstNameLetter}${surnameInLowerCase}`;
}

function getPassword() {
    const login = this.getLogin();
    const year = this.birthday.split('.')[2];

    return `${login}${year}`;
}

function getAge() {
    const birthday = this.birthday;
    const dateParts = birthday.split(".");
    const birthdayDate = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    return dateDiff(birthdayDate)
}

function createNewUser() {
    const firstName = prompt('What is your name?');
    const lastName = prompt('What is your last name?');
    const birthday = prompt('When you were born? (dd.mm.yyyy)');

    const newUser = { firstName, lastName, birthday };

    newUser.getLogin = getLogin;
    newUser.getAge = getAge;
    newUser.getPassword = getPassword;

    return newUser;
}

const user1 = createNewUser();
const user1Age = user1.getAge();
const user1Password = user1.getPassword();

console.log(user1);
console.log(user1Age);
console.log(user1Password);
